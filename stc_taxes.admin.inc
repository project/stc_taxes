<?php

/**
 * Builds main admin congiguration form for stc_taxes module.
 * 
 * @return
 * A forms API form array.
 */
function stc_taxes_admin_settings_form() {
  $form = array();
  
  if (!count($_POST)) {
    if ((variable_get('stc_taxes_rate_retrieval_method', 'database') == 'database' || variable_get('stc_taxes_use_db_rates_as_backup', 0)) & stc_taxes_data_table_is_empty()) {
      drupal_set_message(t('You have currently configured this module to use the database to retrieve tax rates, but the database table is empty.  Please !import data from theSTC.com first.  No tax rates will be retrieved from the database if none are available.', array('!import' => l(t('import'), 'admin/store/settings/stc_taxes/import_data'))), 'error');
    }
  }
  
  $form['stc_taxes_rate_retrieval'] = array(
    '#type' => 'fieldset',
    '#title' => t('Methods for retrieving tax rates'),
    '#theme' => 'stc_taxes_rate_retrieval_form'
  );
  
  $stc_taxes_rate_retrieval_method = variable_get('stc_taxes_rate_retrieval_method', 'database');
  
  $form['stc_taxes_rate_retrieval']['stc_taxes_rate_retrieval_method'] = array(
    '#type' => 'radios',
    '#options' => array(
      'database' => t('Rely only on theSTC.com data stored in the local database.'),
      'webservice' => t('Use theSTC.com web-service to retrieve tax rates.'),
    ),
    '#default_value' => $stc_taxes_rate_retrieval_method
  );
  
  $form['stc_taxes_rate_retrieval']['stc_taxes_MID'] = array(
    '#prefix' => '<div style="margin-left: 40px">',
    '#type' => 'textfield',
    '#title' => t('Your theSTC.com Merchant ID (MID)'),
    '#default_value' => variable_get('stc_taxes_MID', ''),
    '#description' => t('Enter your theSTC.com merchant ID to be used for retrieving rates via web-service.'),
    '#suffix' => '</div>'
  );
  
  $form['stc_taxes_rate_retrieval']['stc_taxes_use_db_rates_as_backup'] = array(
    '#prefix' => '<div style="margin-left: 40px">',
    '#type' => 'checkbox',
    '#title' => t('Use tax-rates provided by theSTC.com and stored in your database as back-up in case web retrieval of rates doesn\'t work.'),
    '#default_value' => variable_get('stc_taxes_use_db_rates_as_backup', 0),
    '#suffix' => '</div>'
  );
  
  $form['stc_taxes_keep_records'] = array(
    '#type' => 'checkbox',
    '#title' => t('Keep records of taxes that are applied.'),
    '#default_value' => variable_get('stc_taxes_keep_records', 1),
    '#return_value' => 1,
    '#description' => t('This is useful if tax rates change or if an order and the tax amount need to be re-constructed later.')
  );
  
  $form['stc_taxes_states_with_shipping_tax'] = stc_taxes_states_checkboxes(variable_get('stc_taxes_states_with_shipping_tax', array()));
  $form['stc_taxes_states_with_shipping_tax']['#type'] = 'fieldset';
  $form['stc_taxes_states_with_shipping_tax']['#tree'] = TRUE;
  $form['stc_taxes_states_with_shipping_tax']['#theme'] = 'stc_taxes_states_checkboxes';
  $form['stc_taxes_states_with_shipping_tax']['#title'] = t('Apply sales tax to shipping charges for following states/territories');
  $form['stc_taxes_states_with_shipping_tax']['#description'] = t('Check boxes for states and territories if you need to apply the state-level sales tax rate to shipping charges.');
  $form['stc_taxes_states_with_shipping_tax']['#collapsible'] = TRUE;
  $form['stc_taxes_states_with_shipping_tax']['#collapsed'] = TRUE;
  
  $form['sales_tax'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings for sales tax application'),
    '#theme' => 'stc_taxes_application_config_form'
  );
  
  $form['sales_tax']['stc_taxes_sales_tax_product_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Taxed product types'),
    '#multiple' => TRUE,
    '#options' => uc_product_type_names(),
    '#default_value' => variable_get('stc_taxes_sales_tax_product_types', array()),
    '#description' => t('Apply taxes to specific product types.'),
  );
  
  $form['use_tax'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings for use tax application'),
    '#theme' => 'stc_taxes_application_config_form',
    '#description' => t('IMPORTANT: Please note that use tax retrieval does not work when using the web-service.  If you plan on applying use-taxes, you are strongly advised to use only the database for rate retrieval or to use this module in conjunction with another module that retrieves and applies use taxes.')
  );
  
  $form['use_tax']['stc_taxes_use_tax_product_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Taxed product types'),
    '#multiple' => TRUE,
    '#options' => uc_product_type_names(),
    '#default_value' => variable_get('stc_taxes_use_tax_product_types', array()),
    '#description' => t('Apply taxes to specific product types.'),
  );
  
  // offer configuration settings to determine which product types get taxed in which states
  foreach (array('sales_tax', 'use_tax') as $tax_type) {
    $product_type_setting = 'stc_taxes_'. $tax_type .'_product_types';
    foreach ($form[$tax_type][$product_type_setting]['#options'] as $product_type => $product_type_name) {
      $var_name = 'stc_taxes_'. $tax_type .'_included_states4'. $product_type;
      $form[$tax_type][$var_name] = stc_taxes_states_checkboxes(variable_get($var_name, stc_taxes_states_default_selection()));
      $form[$tax_type][$var_name]['#type'] = 'fieldset';
      $form[$tax_type][$var_name]['#tree'] = TRUE;
      $form[$tax_type][$var_name]['#theme'] = 'stc_taxes_states_checkboxes';
      $form[$tax_type][$var_name]['#title'] = t("Choose states for which to charge a sales tax on the '!prod_class' product class", array('!prod_class' => $product_type_name));
      $form[$tax_type][$var_name]['#collapsible'] = TRUE;
      $form[$tax_type][$var_name]['#collapsed'] = TRUE;
      $form[$tax_type][$var_name]['#prefix'] = '<div style="margin-left: 40px;">';
      $form[$tax_type][$var_name]['#suffix'] = '</div>';
    }
  }
  
  return system_settings_form($form);
}

/**
 * Helper function that tells whether or not the {stc_tax_rates} DB table is empty.
 * Don't want to run this query twice, so we store result in static variable.
 */
function stc_taxes_data_table_is_empty() {
  static $is_empty;
  
  if (is_null($is_empty)) {
    $count = db_result(db_query("SELECT COUNT(*) AS count FROM {stc_tax_rates}"));
    
    if (!$count) {
      $is_empty = TRUE;
    }
    else {
      $is_empty = FALSE;
    }
  }
  
  return $is_empty;
}

/**
 * Returns a page with links for downloading files.
 * Currently only gives one link for downloading taxes applied to each line_item_id
 * and order_product_id
 */
function stc_taxes_data_export_page() {
  $output = '';
  $output .= t('!download a CSV file detailing tax applications.', array('!download' => l(t('Download'), 'admin/store/settings/stc_taxes/export/applications_csv')));
  
  return $output;
}

function stc_taxes_export_applications_data() {
  
  $result = db_query('SELECT stcapp.*, o.order_status FROM {stc_tax_applications} stcapp LEFT JOIN {uc_orders} o ON stcapp.order_id = o.order_id ORDER BY order_id, application_type DESC, application_id');
  
  header('Content-type: application/csv');
  header('Content-Disposition: attachment; filename="tax_application_data.csv"');
  
  // the first line of the CSV; column names
  $first_line = array();
  $first_line[] = 'order_id';
  $first_line[] = 'order_status';
  $first_line[] = 'application_type';
  $first_line[] = 'application_id';
  $first_line[] = 'city_sales_rate';
  $first_line[] = 'county_sales_rate';
  $first_line[] = 'state_sales_rate';
  $first_line[] = 'city_use_rate';
  $first_line[] = 'county_use_rate';
  $first_line[] = 'state_use_rate';
  
  print implode(",", $first_line) ."\n";
  
  while ($row = db_fetch_object($result)) {
    $line = array();
    foreach ($first_line as $field) {
      $line[] = $row->$field;
    }
    print implode(",", $line) ."\n";
  }
  
  exit;
}

/**
 * Returns an array of checkboxes with a checkbox for each US state/territory.
 * Re-used when adding options to admin config form for deciding state-by-state 
 * configurations for different tax types.
 *
 * @param
 * An array of zone_id values as defined in the {uc_zones} table by the uc_store module to
 * use as default selections for the config setting option for which this fu is being called.
 *
 * @return
 * An array of forms API arrays, each being a checkbox for a US state/territory.
 */
function stc_taxes_states_checkboxes($default_values = array()) {
  // get uc_store module's country_id value for United States
  $country_id = db_result(db_query("SELECT country_id FROM {uc_countries} WHERE country_iso_code_2 = 'US'"));
  
  $states_result = db_query('SELECT zone_id, zone_code FROM {uc_zones} WHERE zone_country_id = %d ORDER BY zone_code', $country_id);
  
  $checkboxes = array();
  
  while ($row = db_fetch_object($states_result)) {
    $checkboxes[$row->zone_id] = array(
      '#type' => 'checkbox',
      '#title' => $row->zone_code,
      '#return_value' => $row->zone_id,
      '#default_value' => $default_values[$row->zone_id]
    );
  }
  
  return $checkboxes;
}

/**
 * Returns page with one form for importing a data file 
 * and another form for importing a DIF data file
 */
function stc_taxes_data_import_forms_page() {
  if ($_SESSION['stc_data_import_completed']) {
    unset($_SESSION['stc_data_import_completed']);
    drupal_set_message(t('Your import has been completed.'));
  }
  
  $output = drupal_get_form('stc_taxes_new_data_file_form');
  
  $output .= drupal_get_form('stc_taxes_update_data_file_form');
  
  return $output;
}

/**
 * Returns a forms array for importing a data file from theSTC.com
 */
function stc_taxes_new_data_file_form() {
  $form['data_file'] = array(
    '#type' => 'file',
    '#title' => t('Upload a file with new tax data')
  );
  
  $form['#attributes'] = array('enctype' => "multipart/form-data");
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit')
  );
  
  return $form;
}

/**
 * Returns a forms array for importing a data file from theSTC.com that updates tax-rate data.
 */
function stc_taxes_update_data_file_form() {
  $form['data_file'] = array(
    '#type' => 'file',
    '#title' => t('Upload a file for updating data')
  );
  
  $form['#attributes'] = array('enctype' => "multipart/form-data");
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit')
  );
  
  return $form;
}

/**
 * Forms API submit handler for the form w/ form_id 'stc_taxes_new_data_file_form'
 */
function stc_taxes_new_data_file_form_submit($form, &$form_state) {
  $result = file_save_upload('data_file', array(), FALSE, FILE_EXISTS_REPLACE);
  
  if ($result) {
    $_SESSION['stc_taxes_data_file'] = $result;
    drupal_goto('admin/store/settings/stc_taxes/import_data/new');
  }
}

/**
 * Forms API submit handler for the form w/ form_id 'stc_taxes_update_data_file_form'
 */
function stc_taxes_update_data_file_form_submit($form, &$form_state) {
  $result = file_save_upload('data_file', array(), FALSE, FILE_EXISTS_REPLACE);
  
  if ($result) {
    $_SESSION['stc_taxes_data_file'] = $result;
    drupal_goto('admin/store/settings/stc_taxes/import_data/update');
  }
}

function stc_taxes_loop_through_data_file($type = 'new') {
  if ($type == 'new') {
    $parse_write_function = 'stc_taxes_nd_write_new_line2db';
  }
  else {
    $parse_write_function = 'stc_taxes_nd_write_update_line2db';
  }
  
  include_once(drupal_get_path('module', 'stc_taxes') .'/stc_taxes_parse_new_data.php');
  
  if ($_SESSION['stc_taxes_data_file']) {
    $handle = fopen($_SESSION['stc_taxes_data_file']->filepath, 'r');
  }
  else {
    return '<p>'. t('You have reached this page in error.  Please inform the site administrator.') .'</p>';
  }
  
  if ($_SESSION['new_data_file_offset']) {
    if ($_SESSION['stc_taxes_data_file']) {
      fseek($handle, $_SESSION['new_data_file_offset']);
    }
  }
  
  $counter = 0;
  $start_time = time();
  while ($line = fgets($handle)) {
    $counter++;
    $parse_write_function($line);
    
    if ($counter % 512 == 0) {
      $current_time = time();
      if ($current_time - $start_time > 15) {
        $_SESSION['new_data_file_offset'] = ftell($handle);
        fclose($handle);
        $output = '<p>Importing data; Please do not interupt until completion...</p>';
        $percent_done = round(100 * ($_SESSION['new_data_file_offset'] / filesize($_SESSION['stc_taxes_data_file']->filepath)));
        
        $output .= '<p>'. $percent_done .'% completed so far'.'</p>';
        $output .= theme('stc_taxes_import_progress_bar', $percent_done);
        
        $url = url('admin/store/settings/stc_taxes/import_data/new');
        drupal_set_html_head('<meta http-equiv="refresh" content="0; url='. $url .'" />');
        
        return $output;
      }
    }
  }
  
  $_SESSION['stc_data_import_completed'] = 1;
  unset($_SESSION['stc_taxes_data_file']);
  unset($_SESSION['new_data_file_offset']);
  $url = url('admin/store/settings/stc_taxes/import_data');
  drupal_set_html_head('<meta http-equiv="refresh" content="0; url='. $url .'" />');
  
  $output = '<p>'. t('Done!') .'</p>';
  $output .= theme('stc_taxes_import_progress_bar', 100);
  $output .= t('Now redirecting you back to the data import page...');
  
  return $output;
}

/**
 * Returns a progress bar given an integer percentage.
 * @param $percentage
 *   An integer less than or equal to 100.
 * 
 * @return
 *   The HTML string for the progress bar.
 */
function theme_stc_taxes_import_progress_bar($percentage) {
  drupal_add_css(drupal_get_path('module', 'stc_taxes') .'/stc_taxes.admin.css');
  $output = '<div class="stc-admin-bar">'."\n";
  $output .= '<div style="width: '. $percentage .'%;" class="foreground"></div>'."\n";
  $output .= '</div>'."\n";
  
  return $output;
}
