<?php


/* The following functions are for parsing lines of tax rate data from the data provided by theSTC.com */

/* The following description is given by theSTC.com for the format of the data: */

/**
 *
 * The data file is fixed-length ASCII in the following format:
 *
 * ZIP     1     5         5-digit US ZIP code
 * FIPS    6     3         standardized county code, unique within each state
 * ST      9     2         state abbreviation
 * COUNTY  11    25        county name
 * CITY    36    28        city name
 * ST_ST   64    8         state sales tax rate
 * CO_ST   72    8         county sales tax rate
 * CI_ST   80    8         city sales tax rate
 * ST_UT   88    8         state use tax rate
 * CO_UT   96    8         county use tax rate
 * CI_UT   104   8         city use tax rate
 * CRLF    112   2         carriage return/line feed
 *
 * The total sales tax rate for a given ZIP code is the sum of the State rate
 * plus the County Rate plus the City Rate.
 *
 */


/******************************************************************

 Begin definitions of functions that parse various values from a 
 line of theSTC.com data formatted as described above

******************************************************************/

/**
 * Parse and return the FIPS code for the county given in this line of theSTC.com data.
 */
function stc_taxes_nd_parse_fips($line) {
  return substr($line, 5, 3);
}

/**
 * Parse and return the county name in this line of theSTC.com data.
 */
function stc_taxes_nd_parse_county($line) {
  $county = substr($line, 10, 25);
  return trim($county);
}

/**
 * Parse and return the zip code in this line of theSTC.com data.
 */
function stc_taxes_nd_parse_zip($line) {
  return substr($line, 0, 5);
}

/**
 * Parse and return the all-capitals postal state name abbreviation in this line of theSTC.com data.
 */
function stc_taxes_nd_parse_state($line) {
  return substr($line, 8, 2);
}

/**
 * Parse and return the city name in this line of theSTC.com data.
 */
function stc_taxes_nd_parse_city($line) {
  $city = substr($line, 35, 28);
  return trim($city);
}

/**
 * Parse and return a decimal string for the sales tax at the city level for this zip code.
 */
function stc_taxes_nd_parse_city_sales_rate($line) {
  return substr($line, 79, 8);
}

/**
 * Parse and return a decimal string for the sales tax at the county level for this zip code.
 */
function stc_taxes_nd_parse_county_sales_rate($line) {
  return substr($line, 95, 8);
}

/**
 * Parse and return a decimal string for the sales tax at the state level for this zip code.
 */
function stc_taxes_nd_parse_state_sales_rate($line) {
  return substr($line, 63, 8);
}

/**
 * Parse and return a decimal string for the use tax at the city level for this zip code.
 */
function stc_taxes_nd_parse_city_use_rate($line) {
  return substr($line, 103, 8);
}

/**
 * Parse and return a decimal string for the use tax at the city level for this zip code.
 */
function stc_taxes_nd_parse_county_use_rate($line) {
  return substr($line, 95, 8);
}

/**
 * Parse and return a decimal string for the use tax at the state level for this zip code.
 */
function stc_taxes_nd_parse_state_use_rate($line) {
  return substr($line, 87, 8);
}

/******************************************************************

 End definitions of parsing functions

******************************************************************/

/**
 * The following function looks at a line of new data and writes it to the database.
 */
function stc_taxes_nd_write_new_line2db($line) {
  $data = array(
    'zip' => stc_taxes_nd_parse_zip($line),
    'city' => stc_taxes_nd_parse_city($line),
    'county' => stc_taxes_nd_parse_county($line),
    'FIPS' => stc_taxes_nd_parse_fips($line),
    'state' => stc_taxes_nd_parse_state($line),
    'city_sales_rate' => stc_taxes_nd_parse_city_sales_rate($line),
    'county_sales_rate' => stc_taxes_nd_parse_county_sales_rate($line),
    'state_sales_rate' => stc_taxes_nd_parse_state_sales_rate($line),
    'city_use_rate' => stc_taxes_nd_parse_city_use_rate($line),
    'county_use_rate' => stc_taxes_nd_parse_county_use_rate($line),
    'state_use_rate' => stc_taxes_nd_parse_state_use_rate($line)
  );
  
  //print_r($data);
  //print "\n\n---------------------------------------------\n\n";
  
  db_query("DELETE FROM {stc_tax_rates} WHERE zip = '%s' AND city = '%s' AND county = '%s' AND state = '%s'", $data['zip'], $data['city'], $data['county'], $data['state']);
  
  db_query("INSERT INTO {stc_tax_rates} (
      zip,
      city,
      county,
      FIPS,
      state,
      city_sales_rate,
      county_sales_rate,
      state_sales_rate,
      city_use_rate,
      county_use_rate,
      state_use_rate
    ) VALUES (
      '%s',
      '%s',
      '%s',
      '%s',
      '%s',
      '%f',
      '%f',
      '%f',
      '%f',
      '%f',
      '%f'
    )",
    $data['zip'],
    $data['city'],
    $data['county'],
    $data['FIPS'],
    $data['state'],
    $data['city_sales_rate'],
    $data['county_sales_rate'],
    $data['state_sales_rate'],
    $data['city_use_rate'],
    $data['county_use_rate'],
    $data['state_use_rate']
  );
}


function stc_taxes_nd_write_update_line2db($line) {
  $op = substr($line, 0, 3);
  $line = substr($line, 3);
  
  $data = array(
    'zip' => stc_taxes_nd_parse_zip($line),
    'city' => stc_taxes_nd_parse_city($line),
    'county' => stc_taxes_nd_parse_county($line),
    'FIPS' => stc_taxes_nd_parse_fips($line),
    'state' => stc_taxes_nd_parse_state($line),
    'city_sales_rate' => stc_taxes_nd_parse_city_sales_rate($line),
    'county_sales_rate' => stc_taxes_nd_parse_county_sales_rate($line),
    'state_sales_rate' => stc_taxes_nd_parse_state_sales_rate($line),
    'city_use_rate' => stc_taxes_nd_parse_city_use_rate($line),
    'county_use_rate' => stc_taxes_nd_parse_county_use_rate($line),
    'state_use_rate' => stc_taxes_nd_parse_state_use_rate($line)
  );
  
  //print_r($data);
  //print "\n\n---------------------------------------------\n\n";
  if ($op == 'DEL') {
    db_query("DELETE FROM {stc_tax_rates} WHERE zip = '%s' AND city = '%s' AND county = '%s' AND state = '%s'", $data['zip'], $data['city'], $data['county'], $data['state']);
  }
  else {
    db_query("UPDATE {stc_tax_rates} SET
        city_sales_rate  = %f,
        county_sales_rate = %f,
        state_sales_rate = %f,
        city_use_rate = %f,
        county_use_rate = %f,
        state_use_rate = %f
      WHERE
        zip = '%s' AND
        city = '%s' AND
        county = '%s' AND
        state = '%s'
      ",
      $data['city_sales_rate'],
      $data['county_sales_rate'],
      $data['state_sales_rate'],
      $data['city_use_rate'],
      $data['county_use_rate'],
      $data['state_use_rate'],
      $data['zip'],
      $data['city'],
      $data['county'],
      $data['state']
    );
  }
}