INTRODUCTION
--
This module allows you to use the theSTC.com tax-rate lookup web-service.  The web-service uses the customer's zipcode, city, and state/territory to determine the tax that should be applied.  The configuration allows you to decide whether or not to apply a tax per product, per state.  Finally, the module also allows you to configure which states require a state sales tax on shipping.

This module is invoked by the core uc_taxes module.  That means you can use this module and the tax rule/condition interface provided by the ubercart core uc_taxes module at the same time.  Just be careful not to set up redundant taxes where both modules end up applying the same tax to a product or shipping fee.

LIMITATIONS
--
Currently, this module only allows you to get tax rates for the United States.

Furthermore, the web-service option does not determine use-tax rates, only sales tax rates.  So if you plan on using this module for use-tax rates, you are strongly encouraged to configure this module to use only the database with the licensed data files you get from theSTC.com.

INSTALLATION
--
Install this module by enabling it on the modules page (url: admin/build/modules).

CONFIGURATION
--
This module allows you to configure sales and use taxes, per product, per state.  To get to the administration section, go to Admin => Store => Settings (url: admin/store/settings).  From there, you can go to the configuration page for the theSTC.com taxes module.

At the very top, you will be given 2 options for determining web-tax rates: database or web-service (with an optional configuration for using database as a back-up for the web-service).  Using the database means you need to import a tax data file from theSTC.com (see below).  Using the web-service means you need to enter the Merchant ID (MID) assigned to you by 

Next, there will be an expandable section for shipping taxes that displays a table of checkboxes, with one per U.S. state/territory.  By default, none of the states are configured as states that tax shipping.

There will be a section for 'Sales Tax' where you can mark a checkbox next to each product type to which you want to apply sales tax.  Right below each checkbox will be an expandable table of checkboxes that allow you to decide the states/territories in which the product tax will be applied.  By default, all of the states are selected.

Lastly, there will be a section that allows you to configure a 'Use Tax', per product, per state, in the same way you configure them for the sales tax.

IMPORTING TAX DATA
--
On this module's configuration page, there is an additional tab for importing data.  There are two types of data files from theSTC.com.  One is for importing new data and another makes modifications to data already in the database.  For this, there are two different file-upload fields.  Uploading files with new data may take some time for two reasons: (1) These files are roughly 4-5 MB in size and (2) There are around 45,000 zipcodes.  BE SURE to make sure your server's PHP settings allow you to upload files of that size.  Most configurations by default only allow a maximum file upload size of 2MB.

POSSIBLE FUTURE EXTENSIONS
--
Currently, this module records to the database the city-/county-/state-level use and sales tax rates that were applied to each product and shipping line item.  However, there is currently no interface for bringing up this data.

Also, there is no interface for looking up the data currently in the database.  An additional tab, on the configuration page, that allows you to query the tax-rate table for this module might be a good aid for users who just want to search on this data, possibly to manually correct an order or for other reference purposes.
